# client-employees



## Para iniciar
Lo primero que debe tener en cuenta para iniciar el proyecto es instalar las dependencias, para esto use: 
<strong>npm install</strong>

Una vez instalada todas las dependencias inicie el servidor local usando:
<strong>npm run dev</strong>

Si todo va bien se espera que el servidor se inicie en el puerto:
<strong> <a href="http://localhost:8000">http://localhost:8000</a> </strong>
